// author Dinesh

"use strict"

const express = require("express");

const ejs = require("ejs") 

const bodyparser = require("body-parser")

const port =8080

const app = express();

require("./model/mongoconnection")

const multipart = require('connect-multiparty')

const multipartMiddleware = multipart()

const path = require('path')

const os = require('os')

const fs = require('fs')

const mongoose = require("mongoose")

const urlModel = mongoose.model("url")

app.use(bodyparser.urlencoded({extended:true}))

app.use(bodyparser.json({extended:true}))

app.use(express.static("src"))

app.set("view engine",'ejs')

app.get("/",(req,res)=>{

    res.render("index")
})

app.post('/', multipartMiddleware, function(req, res) {    
     console.log('files', req.files.data.path)
    let location = path.join(os.tmpdir())    
    console.log(`upload successful, file written to ${location}`) 
        
    let url ={
        url:location
    }
    urlModel.create(url)

    res.send(`upload successful, file written to ${location}`)
  })

app.listen(port,()=>{
    console.log("server running on port",port);    
});
